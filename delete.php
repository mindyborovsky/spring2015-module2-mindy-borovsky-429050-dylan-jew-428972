<?php
	session_start();

  // Check for valid log in
  if (!isset($_SESSION['username'])) {
    header("Location: FileSharing.html");
    exit;
  }

  $username = $_SESSION['username'];

  // Check for valid file for deletion
  if (!isset($_GET['filedelete'])) {
    header("Location: userview.php");
    exit;
  }

  // Filter file name to be deleted
  $filename = $_GET['filedelete'];
  if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
    echo "Invalid filename";
    exit;
  }


    $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
  if (file_exists($full_path)){
    if (unlink($full_path)) {
      header("Location: userview.php");
      exit;
    } else {
      echo "Error deleting file.";
      exit;
    }
  }
  else {
    echo "File doesn't exist";
    echo "<br>";
    echo '<a href="userview.php">Back to User Page</a>';
  }

	
?>