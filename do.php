<!DOCTYPE html>
<html>
	<head>
		<title>Files</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php

		// Check for valid log in
		if (!isset($_GET['username'])) {
			header("Location: FileSharing.html");
		}

		// cast get username to string
		$username = (string) $_GET['username'];

		// Filter input, Link back to log in if invalid
		if( !preg_match('/^[\w_\-]+$/', $username) ){
			echo 'Invalid username<br>';
			echo '<a href="FileSharing.html">Back to Log In Page</a>';
		} else {
			$array = array();
			$userFile = "/srv/users/users.txt";
			$h = fopen($userFile, "r");
			$linenum = 1;
			while (!feof($h)){
				$array[$linenum] = trim(fgets($h));
				++$linenum;
			}
			fclose($h);

			$match = false;
			for ($i =1; $i < $linenum; $i++){
				if ($array[$i] == $username){
					$match = true;
					break;
				}
			}

			if ($match){
				session_start();
				$_SESSION["baseurl"] = "http://" . $_SERVER['SERVER_NAME'] . "/";
				$url =  $_SESSION["baseurl"] . "userview.php";
				$_SESSION["username"] = $username;
				$_SESSION['uploadUrl'] = "/srv/uploads/";
				header("Location: " . $url);
				exit();
			}
		}
			printf("<p>I'm sorry %s is not a registered username. Please try again</p>", $username);
			echo ("<br>");
			echo '<a href="FileSharing.html">Back to Log In Page</a>';
		?>
	</body>
</html>