<?php
session_start();

// Check for valid log in
if (!isset($_SESSION['username'])) {
  header("Location: FileSharing.html");
  exit;
}

$username = $_SESSION['username'];

// Filter uploaded file
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$match = false;

if (!empty($_POST['usersend'])) {
	$usertosend = $_POST['usersend'];
  	if(!preg_match('/^[\w_\-]+$/', $usertosend) ){
			echo 'Invalid username<br>';
			echo '<a href="userview.php">Back to user page</a>';
	}
	
	$array = array();
	$userFile = "/srv/users/users.txt";
	$h = fopen($userFile, "r");
	$linenum = 1;
	while (!feof($h)){
		$array[$linenum] = trim(fgets($h));
		++$linenum;
	}
	fclose($h);

	
	for ($i =1; $i < $linenum; $i++){
		if ($array[$i] == $username){
			$match = true;
			break;
		}
	}

	if ($match){
		$username = $usertosend;
	}
	else {
		echo 'This user is not registered<br>';
		echo '<a href="userview.php">Back to user page</a>';
		exit;
	}
}


$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	if ($match) {
		printf("Successfully uploaded %s to %s", $filename, $username);
		echo '<br><a href="userview.php">Back to user page</a>';
		exit;
	} else {
		header("Location: userview.php");
		exit;
	}
}else{
	printf("Upload failed.");
	exit;
}

?>