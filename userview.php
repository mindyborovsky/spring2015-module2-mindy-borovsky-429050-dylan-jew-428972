<!DOCTYPE html>
<html>
	<head>
		<title>User Files</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php
		session_start();

		// Check for valid log in
		if (!isset($_SESSION['username'])) {
  		header("Location: FileSharing.html");
  		exit;
		}

		printf( "<h1> Current User: %s </h1>", $_SESSION['username']);
		$dir = new DirectoryIterator($_SESSION['uploadUrl'] . $_SESSION["username"]);
		echo "<ul>";
		foreach($dir as $fileinfo){
			if (!$fileinfo->isDot()){
				$filename = $fileinfo->getFilename();
				echo '<li>'. $filename . "</li>";
			}
		}
		echo "</ul>";
		?>

		<form enctype="multipart/form-data" action= <?php echo $_SESSION["baseurl"] . "upload.php" ?> method="POST">
			<p>
				<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
				<label for="uploadfile_input">Choose a file to upload:</label>
				<input name="uploadedfile" type="file" id="uploadfile_input" />
			</p>
			<p>
				<label for="usersendinput">User to send file to (leave empty to upload to your own user):</label>
				<input type="text" name = "usersend" id = "usersendinput"/>
			</p>
			<p>
				<input type="submit" value="Upload File" />
			</p>
		</form>

		<form action = <?php echo $_SESSION["baseurl"] . "delete.php" ?>  method = "GET">
			<p>
				<label for="filedeleteinput">Delete file (type name):</label>
				<input type="text" name = "filedelete" id = "filedeleteinput"/>
			</p>
			<p>
				<input type="submit" value="Send" />
				<input type="reset"/>
			</p>
		</form>

		<form action = <?php echo $_SESSION["baseurl"] . "view.php" ?> method = "GET">
			<p>
				<label for="fileviewinput"> View listed file (type name):</label>
				<input type = "text" name = "fileview" id = "fileviewinput"/>
				<input type = "submit" value="View"/>
			</p>
		</form>

		<form action = "logout.php" method = "GET">
			<p>
				<label for="logout">Logout</label>
				<input type = "submit" value="Log off" id="logout"/>
			</p>
		</form>

	</body>
</html>