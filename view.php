<?php
session_start();

// Check for valid log in
if (!isset($_SESSION['username'])) {
  header("Location: FileSharing.html");
  exit;
}

// Check for valid file to read
if (!isset($_GET['fileview'])) {
  header("Location: userview.php");
  exit;
}

// filter file to be read
$filename = (string) $_GET['fileview'];
if (!preg_match('/^[\w_\.\-]+$/', $filename)){
	echo "Invalid filename";
	exit;
}

$username = $_SESSION['username'];
if (!preg_match('/^[\w_\.\-]+$/', $username)){
	echo "Invalid username";
	exit;
}

$full_path = $_SESSION['uploadUrl'] . $username . "/" . $filename;

if (file_exists($full_path)){
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$mime = $finfo->file($full_path);
  $download = true;
  switch ($mime) {
    case 'text/plain':
    case 'text/html':
    case 'text/css':
    case 'application/xml':
    case 'image/png':
    case 'image/jpeg':
    case 'image/gif':
    case 'image/bmp':
    case 'image/tiff':
    case 'application/pdf':
      $download = false;
      break;
    default:
  }
  if ($download) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($full_path));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($full_path));
    readfile($full_path);
    exit;
  } else {
	header("Content-Type: " .$mime);
	readfile($full_path);
  }
}
else {
	echo "File does not exist for this user";
	echo '<a href="userview.php">Back to User Page</a>';
}



?>